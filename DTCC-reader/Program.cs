﻿using System;
using System.Net;
//using CME_reader;
using System.IO;
using System.IO.Compression;
using FreeDataModel;
using System.Linq;
using System.Collections.Generic;
using System.Globalization;
using Microsoft.VisualBasic.FileIO;

namespace DTCC_reader
{
    class Program
    {
        //private RawModelContainer RMC; //can change name of class after integration, also need constructor if we want readonly

        static void Main(string[] args)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string filename = "CUMULATIVE_COMMODITITES_2019_06_30.zip";

            //List<DTCC_RawData> DTCC_RawData = new List<DTCC_RawData>();

            using (WebClient wb = new WebClient()) //download cssv
            {
                wb.DownloadFile(new Uri("https://kgc0418-tdw-data-0.s3.amazonaws.com/slices/CUMULATIVE_COMMODITIES_2019_07_01.zip"), path + "\\" + filename);
            }
            using (ZipArchive zip = ZipFile.OpenRead(path + "\\" + filename))
            {
                foreach(ZipArchiveEntry entry in zip.Entries) //open zip
                {
                    var item = entry.Open();
                    using (TextFieldParser parser = new TextFieldParser(new StreamReader(item)))
                    //using (var sr = new StreamReader(item)) //read csv in zip
                    {
                        parser.TextFieldType = FieldType.Delimited;
                        parser.SetDelimiters(",");
                        var headers = parser.ReadFields();

                        while (!parser.EndOfData)
                        {
                            //var line = sr.ReadLine();
                            //var values = line.Split(','); //get line of csv as a list

                            //string[] split = values[3].Split('"');
                            //DateTime temp_date = DateTime.ParseExact(split[1], "yyyy-MM-dd'T'HH:mm:ss", CultureInfo.InvariantCulture);

                            ////split = values[21].Split('"');
                            ////var temp = Convert.ToDouble(split[1]);
                            ////Console.WriteLine(temp);
                            //Console.WriteLine(values[21]);

                            string[] values = parser.ReadFields();

                            DTCC_RawData dtcc_new = new DTCC_RawData() //this is horrible someone kill me
                            {
                                Action = values[2],
                                ExecutionTime = DateTime.Parse(values[3]),
                                Cleared = values[4],
                                Collateralization = values[5],
                                End_User_Exception = values[6],
                                OtherPriceAffecting = values[7] == "N" ? false : true,
                                BlockOff = values[8] == "N" ? false : true,
                                ExecutionVenue = values[9],
                                EffectiveDate = values[10],
                                MaturityDate = values[11],
                                SettleCurrency = values[13],
                                Taxonomy = values[16],
                                Underlying = values[18],
                                PriceNotationType = values[20],
                                PriceNotation = values[21] == ""? (double?)null : Convert.ToDouble(values[21]),
                                NotionalCurrency1 = values[24],
                                NotionalCurrency2 = values[25],
                                NotionalAmount1 = values[26] == "" ? (double?)null : Convert.ToDouble(values[26].Replace(",","").Replace("+","")),
                                NotionalAmount2 = values[27] == "" ? (double?)null : Convert.ToDouble(values[27].Replace(",", "").Replace("+", "")),
                                Strike = values[33] == "" ? (double?)null : Convert.ToDouble(values[33]),
                                IsCall = values[34] == "C-" ? "call" : "put",
                                OptionCurrency = values[36],
                                OptionType = values[35],
                                OptionPremium = values[37] == "" ? (double?)null : Convert.ToDouble(values[37]),
                                OptionExpiration = values[39] == ""? (DateTime?)null : Convert.ToDateTime(values[39]),
                            };

                            //Console.WriteLine(dtcc_new.PriceNotation);

                            using (RawModelContainer RMC = new RawModelContainer())
                            {
                                RMC.DTCC_RawData.Add(dtcc_new);
                                RMC.SaveChanges();
                            }
                        }
                    }
                }
            }

            Console.ReadKey();
        }
    }

    //below was a good idea to copy props but I dont know enough about generics

    //public class PropCopy<TObject, TList> where TObject : DTCC_RawData where TList : List<string>
    //{
    //    public static void Copy(TObject @object, TList list)
    //    {
    //        var ObjectProperties = @object.GetType().GetProperties();
    //        foreach(string item in list)
    //        {
    //            foreach(var objprop in ObjectProperties)
    //            {
    //            }
    //        }
    //    }
    //}
}
