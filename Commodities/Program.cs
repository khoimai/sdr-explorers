﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using FreeDataModel;

namespace Commodities
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch sp = new Stopwatch();
            sp.Start();
            //Model1Container DMC = new Model1Container();

            List<string> comm = new List<string>();
            List<string> product = new List<string>();
            //try
            //{
            //    using(StreamReader sr = new StreamReader(File.OpenRead(@"C:\Users\bugin\Desktop\Projects\sdr-explorer\Commodities.csv")))
            //    {
            //        //string line = sr.ReadToEnd();
            //        while (!sr.EndOfStream)
            //        {
            //            string line = sr.ReadLine();
            //            comm.Add(line);
            //            //Console.WriteLine(line);
            //        }
            //    }
            //    using(StreamReader sr = new StreamReader(File.OpenRead(@"C:\Users\bugin\Desktop\Projects\sdr-explorer\data_exploration\products.csv")))
            //    {
            //        while(!sr.EndOfStream)
            //        {
            //            string line = sr.ReadLine();
            //            product.Add(line);
            //        }
            //    }
            //}
            //catch(IOException e)
            //{
            //    Console.WriteLine("not read \n" + e.Message);
            //    Console.ReadKey();
            //}

            //awful double foreach loop
            //foreach(string p in product)
            //{
            //    foreach(string s in comm)
            //    {
            //        //for each product (trade) check to see if it contains 
            //        //anything from the commodity 'library' comm
            //        if (p.Contains(s))
            //        {
            //            //Console.WriteLine(s + " ... " + p);
            //        }
            //    }
            //}

            //foreach(string s in comm)
            //{
            //    Commodities cm = new Commodities
            //    {
            //        Name = s
            //    };
            //    DMC.Commodities.Add(cm);
            //}
            //foreach(string s in product)
            //{
            //    TradeProducts tp = new TradeProducts()
            //    {
            //        Product = s
            //    };
            //    DMC.TradeProducts.Add(tp);
            //}

            //DMC.SaveChanges();

            CommodityLibrary CommLib = new CommodityLibrary();

            //CommLib.ConvertProduct();

            sp.Stop();
            Console.WriteLine(sp.Elapsed);
            Console.ReadKey();

        }
    }

    public sealed class CommodityLibrary
    {
        private readonly List<string> Commodities;
        //private Model1Container dmc;
        private RawModelContainer RMC;
        public CommodityLibrary()
        {
            //read commodities table in db
            using(RMC = new RawModelContainer())
            {
                //Commodities = (from i in dmc.Commodities select i).ToList();
                Commodities = (from i in RMC.Commodities select i.Name).ToList();
            }
        }

        public string ConvertProduct(string trade_product)
        {
            string matched_comm = "";
            //get product column from db
            //or have newest trades input as a list of obj?????
            //foreach (var s in Commodities) Console.WriteLine(s.Name);
            foreach(string c in Commodities)
            {
                if (trade_product.Contains(c))
                {
                    matched_comm = c;
                    break;
                }
            }

            return matched_comm;
        }
    }
}
