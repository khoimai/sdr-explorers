﻿namespace sdr_explorers
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.EndExp = new System.Windows.Forms.Label();
            this.BlockLg = new System.Windows.Forms.Label();
            this.ExeVenue = new System.Windows.Forms.Label();
            this.Cleared = new System.Windows.Forms.Label();
            this.SettleMethod = new System.Windows.Forms.Label();
            this.SettleCurrency = new System.Windows.Forms.Label();
            this.Effect = new System.Windows.Forms.Label();
            this.DataSource = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.OpPremium = new System.Windows.Forms.Label();
            this.OpCurrency = new System.Windows.Forms.Label();
            this.OpStrike = new System.Windows.Forms.Label();
            this.OpExp = new System.Windows.Forms.Label();
            this.OpIsCall = new System.Windows.Forms.Label();
            this.OpType = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.L2FixPay = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.L2Spread = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.L2Index = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.L2FixPayCurrency = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.L2Type = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.L2TotalNotionUnit = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.L2TotalNational = new System.Windows.Forms.Label();
            this.L1TotalNotional = new System.Windows.Forms.Label();
            this.L1Spread = new System.Windows.Forms.Label();
            this.L1TotalNotionUnit = new System.Windows.Forms.Label();
            this.L1Index = new System.Windows.Forms.Label();
            this.L1Type = new System.Windows.Forms.Label();
            this.L1FixPayCurrency = new System.Windows.Forms.Label();
            this.L1FixPay = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.IdCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MarketCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PriceCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MaturityCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExecutionTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EventCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CollateralizationCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(13, 15);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1066, 548);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Location = new System.Drawing.Point(4, 28);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4, 4, 8, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage1.Size = new System.Drawing.Size(1058, 516);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "CME";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.EndExp);
            this.groupBox1.Controls.Add(this.BlockLg);
            this.groupBox1.Controls.Add(this.ExeVenue);
            this.groupBox1.Controls.Add(this.Cleared);
            this.groupBox1.Controls.Add(this.SettleMethod);
            this.groupBox1.Controls.Add(this.SettleCurrency);
            this.groupBox1.Controls.Add(this.Effect);
            this.groupBox1.Controls.Add(this.DataSource);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Location = new System.Drawing.Point(7, 241);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1043, 268);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Trade Details";
            // 
            // EndExp
            // 
            this.EndExp.AutoSize = true;
            this.EndExp.Location = new System.Drawing.Point(907, 104);
            this.EndExp.Name = "EndExp";
            this.EndExp.Size = new System.Drawing.Size(32, 19);
            this.EndExp.TabIndex = 43;
            this.EndExp.Text = "NA";
            // 
            // BlockLg
            // 
            this.BlockLg.AutoSize = true;
            this.BlockLg.Location = new System.Drawing.Point(907, 75);
            this.BlockLg.Name = "BlockLg";
            this.BlockLg.Size = new System.Drawing.Size(32, 19);
            this.BlockLg.TabIndex = 42;
            this.BlockLg.Text = "NA";
            // 
            // ExeVenue
            // 
            this.ExeVenue.AutoSize = true;
            this.ExeVenue.Location = new System.Drawing.Point(907, 49);
            this.ExeVenue.Name = "ExeVenue";
            this.ExeVenue.Size = new System.Drawing.Size(32, 19);
            this.ExeVenue.TabIndex = 41;
            this.ExeVenue.Text = "NA";
            // 
            // Cleared
            // 
            this.Cleared.AutoSize = true;
            this.Cleared.Location = new System.Drawing.Point(907, 23);
            this.Cleared.Name = "Cleared";
            this.Cleared.Size = new System.Drawing.Size(32, 19);
            this.Cleared.TabIndex = 40;
            this.Cleared.Text = "NA";
            // 
            // SettleMethod
            // 
            this.SettleMethod.AutoSize = true;
            this.SettleMethod.Location = new System.Drawing.Point(635, 104);
            this.SettleMethod.Name = "SettleMethod";
            this.SettleMethod.Size = new System.Drawing.Size(32, 19);
            this.SettleMethod.TabIndex = 39;
            this.SettleMethod.Text = "NA";
            // 
            // SettleCurrency
            // 
            this.SettleCurrency.AutoSize = true;
            this.SettleCurrency.Location = new System.Drawing.Point(635, 75);
            this.SettleCurrency.Name = "SettleCurrency";
            this.SettleCurrency.Size = new System.Drawing.Size(32, 19);
            this.SettleCurrency.TabIndex = 38;
            this.SettleCurrency.Text = "NA";
            // 
            // Effect
            // 
            this.Effect.AutoSize = true;
            this.Effect.Location = new System.Drawing.Point(635, 49);
            this.Effect.Name = "Effect";
            this.Effect.Size = new System.Drawing.Size(32, 19);
            this.Effect.TabIndex = 37;
            this.Effect.Text = "NA";
            // 
            // DataSource
            // 
            this.DataSource.AutoSize = true;
            this.DataSource.Location = new System.Drawing.Point(635, 23);
            this.DataSource.Name = "DataSource";
            this.DataSource.Size = new System.Drawing.Size(32, 19);
            this.DataSource.TabIndex = 36;
            this.DataSource.Text = "NA";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(761, 104);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(152, 19);
            this.label27.TabIndex = 35;
            this.label27.Text = "End-User Exception:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(761, 49);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(128, 19);
            this.label25.TabIndex = 34;
            this.label25.Text = "Execution Venue:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(761, 23);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(112, 19);
            this.label24.TabIndex = 33;
            this.label24.Text = "Cleared Status:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(485, 104);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(145, 19);
            this.label23.TabIndex = 32;
            this.label23.Text = "Settlement Method:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(485, 75);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(155, 19);
            this.label22.TabIndex = 31;
            this.label22.Text = "Settlement Currency:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(761, 75);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(142, 19);
            this.label26.TabIndex = 30;
            this.label26.Text = "Block/Lg Notional:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(485, 49);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(113, 19);
            this.label21.TabIndex = 29;
            this.label21.Text = "Effective Date:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(485, 23);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(97, 19);
            this.label11.TabIndex = 28;
            this.label11.Text = "Data Source:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.OpPremium);
            this.groupBox3.Controls.Add(this.OpCurrency);
            this.groupBox3.Controls.Add(this.OpStrike);
            this.groupBox3.Controls.Add(this.OpExp);
            this.groupBox3.Controls.Add(this.OpIsCall);
            this.groupBox3.Controls.Add(this.OpType);
            this.groupBox3.Controls.Add(this.label30);
            this.groupBox3.Controls.Add(this.label29);
            this.groupBox3.Controls.Add(this.label34);
            this.groupBox3.Controls.Add(this.label28);
            this.groupBox3.Controls.Add(this.label31);
            this.groupBox3.Location = new System.Drawing.Point(489, 143);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(496, 119);
            this.groupBox3.TabIndex = 27;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Option Details";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(291, 58);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 19);
            this.label10.TabIndex = 35;
            this.label10.Text = "Currency:";
            // 
            // OpPremium
            // 
            this.OpPremium.AutoSize = true;
            this.OpPremium.Location = new System.Drawing.Point(406, 89);
            this.OpPremium.Name = "OpPremium";
            this.OpPremium.Size = new System.Drawing.Size(32, 19);
            this.OpPremium.TabIndex = 34;
            this.OpPremium.Text = "NA";
            // 
            // OpCurrency
            // 
            this.OpCurrency.AutoSize = true;
            this.OpCurrency.Location = new System.Drawing.Point(406, 58);
            this.OpCurrency.Name = "OpCurrency";
            this.OpCurrency.Size = new System.Drawing.Size(32, 19);
            this.OpCurrency.TabIndex = 33;
            this.OpCurrency.Text = "NA";
            // 
            // OpStrike
            // 
            this.OpStrike.AutoSize = true;
            this.OpStrike.Location = new System.Drawing.Point(406, 28);
            this.OpStrike.Name = "OpStrike";
            this.OpStrike.Size = new System.Drawing.Size(32, 19);
            this.OpStrike.TabIndex = 32;
            this.OpStrike.Text = "NA";
            // 
            // OpExp
            // 
            this.OpExp.AutoSize = true;
            this.OpExp.Location = new System.Drawing.Point(157, 89);
            this.OpExp.Name = "OpExp";
            this.OpExp.Size = new System.Drawing.Size(32, 19);
            this.OpExp.TabIndex = 31;
            this.OpExp.Text = "NA";
            // 
            // OpIsCall
            // 
            this.OpIsCall.AutoSize = true;
            this.OpIsCall.Location = new System.Drawing.Point(157, 58);
            this.OpIsCall.Name = "OpIsCall";
            this.OpIsCall.Size = new System.Drawing.Size(32, 19);
            this.OpIsCall.TabIndex = 30;
            this.OpIsCall.Text = "NA";
            // 
            // OpType
            // 
            this.OpType.AutoSize = true;
            this.OpType.Location = new System.Drawing.Point(157, 28);
            this.OpType.Name = "OpType";
            this.OpType.Size = new System.Drawing.Size(32, 19);
            this.OpType.TabIndex = 29;
            this.OpType.Text = "NA";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(28, 89);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(85, 19);
            this.label30.TabIndex = 28;
            this.label30.Text = "Expiration:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(28, 58);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(54, 19);
            this.label29.TabIndex = 27;
            this.label29.Text = "IsCall:";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(291, 89);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(77, 19);
            this.label34.TabIndex = 26;
            this.label34.Text = "Premium:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(28, 28);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(97, 19);
            this.label28.TabIndex = 23;
            this.label28.Text = "Option Type:";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(291, 28);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(55, 19);
            this.label31.TabIndex = 24;
            this.label31.Text = "Strike:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.L2FixPay);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.L2Spread);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.L2Index);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.L2FixPayCurrency);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.L2Type);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.L2TotalNotionUnit);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.L2TotalNational);
            this.groupBox2.Controls.Add(this.L1TotalNotional);
            this.groupBox2.Controls.Add(this.L1Spread);
            this.groupBox2.Controls.Add(this.L1TotalNotionUnit);
            this.groupBox2.Controls.Add(this.L1Index);
            this.groupBox2.Controls.Add(this.L1Type);
            this.groupBox2.Controls.Add(this.L1FixPayCurrency);
            this.groupBox2.Controls.Add(this.L1FixPay);
            this.groupBox2.Location = new System.Drawing.Point(6, 34);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(477, 228);
            this.groupBox2.TabIndex = 26;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Swap and Forward Details";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // L2FixPay
            // 
            this.L2FixPay.AutoSize = true;
            this.L2FixPay.Location = new System.Drawing.Point(355, 122);
            this.L2FixPay.Name = "L2FixPay";
            this.L2FixPay.Size = new System.Drawing.Size(32, 19);
            this.L2FixPay.TabIndex = 19;
            this.L2FixPay.Text = "NA";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(230, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Leg1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(385, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 23);
            this.label2.TabIndex = 1;
            this.label2.Text = "Leg2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 19);
            this.label3.TabIndex = 2;
            this.label3.Text = "Total Notional:";
            // 
            // L2Spread
            // 
            this.L2Spread.AutoSize = true;
            this.L2Spread.Location = new System.Drawing.Point(355, 184);
            this.L2Spread.Name = "L2Spread";
            this.L2Spread.Size = new System.Drawing.Size(32, 19);
            this.L2Spread.TabIndex = 22;
            this.L2Spread.Text = "NA";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 79);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(146, 19);
            this.label4.TabIndex = 3;
            this.label4.Text = "Total Notional Unit:";
            // 
            // L2Index
            // 
            this.L2Index.AutoSize = true;
            this.L2Index.Location = new System.Drawing.Point(355, 163);
            this.L2Index.Name = "L2Index";
            this.L2Index.Size = new System.Drawing.Size(32, 19);
            this.L2Index.TabIndex = 21;
            this.L2Index.Text = "NA";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 100);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 19);
            this.label5.TabIndex = 4;
            this.label5.Text = "Type:";
            // 
            // L2FixPayCurrency
            // 
            this.L2FixPayCurrency.AutoSize = true;
            this.L2FixPayCurrency.Location = new System.Drawing.Point(355, 142);
            this.L2FixPayCurrency.Name = "L2FixPayCurrency";
            this.L2FixPayCurrency.Size = new System.Drawing.Size(32, 19);
            this.L2FixPayCurrency.TabIndex = 20;
            this.L2FixPayCurrency.Text = "NA";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 163);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 19);
            this.label6.TabIndex = 5;
            this.label6.Text = "Index:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(19, 142);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(182, 19);
            this.label7.TabIndex = 6;
            this.label7.Text = "Fixed Payment Currency:";
            // 
            // L2Type
            // 
            this.L2Type.AutoSize = true;
            this.L2Type.Location = new System.Drawing.Point(355, 100);
            this.L2Type.Name = "L2Type";
            this.L2Type.Size = new System.Drawing.Size(32, 19);
            this.L2Type.TabIndex = 18;
            this.L2Type.Text = "NA";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(19, 122);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(115, 19);
            this.label8.TabIndex = 7;
            this.label8.Text = "Fixed Payment:";
            // 
            // L2TotalNotionUnit
            // 
            this.L2TotalNotionUnit.AutoSize = true;
            this.L2TotalNotionUnit.Location = new System.Drawing.Point(355, 79);
            this.L2TotalNotionUnit.Name = "L2TotalNotionUnit";
            this.L2TotalNotionUnit.Size = new System.Drawing.Size(32, 19);
            this.L2TotalNotionUnit.TabIndex = 17;
            this.L2TotalNotionUnit.Text = "NA";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(19, 184);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(60, 19);
            this.label9.TabIndex = 8;
            this.label9.Text = "Spread:";
            // 
            // L2TotalNational
            // 
            this.L2TotalNational.AutoSize = true;
            this.L2TotalNational.Location = new System.Drawing.Point(355, 58);
            this.L2TotalNational.Name = "L2TotalNational";
            this.L2TotalNational.Size = new System.Drawing.Size(32, 19);
            this.L2TotalNational.TabIndex = 16;
            this.L2TotalNational.Text = "NA";
            // 
            // L1TotalNotional
            // 
            this.L1TotalNotional.AutoSize = true;
            this.L1TotalNotional.Location = new System.Drawing.Point(199, 58);
            this.L1TotalNotional.Name = "L1TotalNotional";
            this.L1TotalNotional.Size = new System.Drawing.Size(32, 19);
            this.L1TotalNotional.TabIndex = 9;
            this.L1TotalNotional.Text = "NA";
            // 
            // L1Spread
            // 
            this.L1Spread.AutoSize = true;
            this.L1Spread.Location = new System.Drawing.Point(199, 184);
            this.L1Spread.Name = "L1Spread";
            this.L1Spread.Size = new System.Drawing.Size(32, 19);
            this.L1Spread.TabIndex = 15;
            this.L1Spread.Text = "NA";
            // 
            // L1TotalNotionUnit
            // 
            this.L1TotalNotionUnit.AutoSize = true;
            this.L1TotalNotionUnit.Location = new System.Drawing.Point(199, 79);
            this.L1TotalNotionUnit.Name = "L1TotalNotionUnit";
            this.L1TotalNotionUnit.Size = new System.Drawing.Size(32, 19);
            this.L1TotalNotionUnit.TabIndex = 10;
            this.L1TotalNotionUnit.Text = "NA";
            // 
            // L1Index
            // 
            this.L1Index.AutoSize = true;
            this.L1Index.Location = new System.Drawing.Point(199, 163);
            this.L1Index.Name = "L1Index";
            this.L1Index.Size = new System.Drawing.Size(32, 19);
            this.L1Index.TabIndex = 14;
            this.L1Index.Text = "NA";
            // 
            // L1Type
            // 
            this.L1Type.AutoSize = true;
            this.L1Type.Location = new System.Drawing.Point(199, 100);
            this.L1Type.Name = "L1Type";
            this.L1Type.Size = new System.Drawing.Size(32, 19);
            this.L1Type.TabIndex = 11;
            this.L1Type.Text = "NA";
            // 
            // L1FixPayCurrency
            // 
            this.L1FixPayCurrency.AutoSize = true;
            this.L1FixPayCurrency.Location = new System.Drawing.Point(199, 142);
            this.L1FixPayCurrency.Name = "L1FixPayCurrency";
            this.L1FixPayCurrency.Size = new System.Drawing.Size(32, 19);
            this.L1FixPayCurrency.TabIndex = 13;
            this.L1FixPayCurrency.Text = "NA";
            // 
            // L1FixPay
            // 
            this.L1FixPay.AutoSize = true;
            this.L1FixPay.Location = new System.Drawing.Point(199, 122);
            this.L1FixPay.Name = "L1FixPay";
            this.L1FixPay.Size = new System.Drawing.Size(32, 19);
            this.L1FixPay.TabIndex = 12;
            this.L1FixPay.Text = "NA";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdCol,
            this.ProductCol,
            this.MarketCol,
            this.PriceCol,
            this.MaturityCol,
            this.ExecutionTimeCol,
            this.EventCol,
            this.CollateralizationCol});
            this.dataGridView1.Location = new System.Drawing.Point(4, 13);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(1046, 221);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // IdCol
            // 
            this.IdCol.HeaderText = "Id";
            this.IdCol.Name = "IdCol";
            this.IdCol.ReadOnly = true;
            // 
            // ProductCol
            // 
            this.ProductCol.HeaderText = "Product";
            this.ProductCol.Name = "ProductCol";
            this.ProductCol.ReadOnly = true;
            this.ProductCol.Width = 120;
            // 
            // MarketCol
            // 
            this.MarketCol.HeaderText = "Market";
            this.MarketCol.Name = "MarketCol";
            this.MarketCol.ReadOnly = true;
            // 
            // PriceCol
            // 
            this.PriceCol.HeaderText = "Price";
            this.PriceCol.Name = "PriceCol";
            this.PriceCol.ReadOnly = true;
            // 
            // MaturityCol
            // 
            this.MaturityCol.HeaderText = "Maturity Date";
            this.MaturityCol.Name = "MaturityCol";
            this.MaturityCol.ReadOnly = true;
            this.MaturityCol.Width = 150;
            // 
            // ExecutionTimeCol
            // 
            this.ExecutionTimeCol.HeaderText = "Execution Time";
            this.ExecutionTimeCol.Name = "ExecutionTimeCol";
            this.ExecutionTimeCol.ReadOnly = true;
            this.ExecutionTimeCol.Width = 150;
            // 
            // EventCol
            // 
            this.EventCol.HeaderText = "Event";
            this.EventCol.Name = "EventCol";
            this.EventCol.ReadOnly = true;
            // 
            // CollateralizationCol
            // 
            this.CollateralizationCol.HeaderText = "Collateralization";
            this.CollateralizationCol.Name = "CollateralizationCol";
            this.CollateralizationCol.ReadOnly = true;
            this.CollateralizationCol.Width = 150;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 28);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage2.Size = new System.Drawing.Size(1031, 516);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Location = new System.Drawing.Point(4, 28);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage3.Size = new System.Drawing.Size(1031, 516);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "tabPage3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1092, 588);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "SDR_Reader";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label L2Spread;
        private System.Windows.Forms.Label L2Index;
        private System.Windows.Forms.Label L2FixPayCurrency;
        private System.Windows.Forms.Label L2FixPay;
        private System.Windows.Forms.Label L2Type;
        private System.Windows.Forms.Label L2TotalNotionUnit;
        private System.Windows.Forms.Label L2TotalNational;
        private System.Windows.Forms.Label L1Spread;
        private System.Windows.Forms.Label L1Index;
        private System.Windows.Forms.Label L1FixPayCurrency;
        private System.Windows.Forms.Label L1FixPay;
        private System.Windows.Forms.Label L1Type;
        private System.Windows.Forms.Label L1TotalNotionUnit;
        private System.Windows.Forms.Label L1TotalNotional;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn MarketCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn PriceCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaturityCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExecutionTimeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn EventCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn CollateralizationCol;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label OpIsCall;
        private System.Windows.Forms.Label OpType;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label OpPremium;
        private System.Windows.Forms.Label OpCurrency;
        private System.Windows.Forms.Label OpStrike;
        private System.Windows.Forms.Label OpExp;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label EndExp;
        private System.Windows.Forms.Label BlockLg;
        private System.Windows.Forms.Label ExeVenue;
        private System.Windows.Forms.Label Cleared;
        private System.Windows.Forms.Label SettleMethod;
        private System.Windows.Forms.Label SettleCurrency;
        private System.Windows.Forms.Label Effect;
        private System.Windows.Forms.Label DataSource;
        private System.Windows.Forms.Label label10;
    }
}

