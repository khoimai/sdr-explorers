﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FluentFTP;
using System.Timers;
using FreeDataModel;
using CME_reader;

namespace sdr_explorers
{
    public partial class Form1 : Form
    {
        RawModelContainer db = new RawModelContainer();
        private int current_Id = 1;

        public Form1()
        {
            InitializeComponent();
            //// Get the first id that we need.
            //current_Id = (from q in db.CME_RawData
            //              select q.Id).Last() + 1;

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //int InitId;
            //// Get the initial id in database at begining.
            //InitId = db.CME_RawData.Max(u => u.Id);

            //// Check if there is any data in database.
            //if (InitId < 1)
            //{
            //    // Pull down data from web.
            //    CMEsdr cme_reader = new CMEsdr();
            //    cme_reader.Connect();
            //    cme_reader.poll_list_of_trades("commodities");
            //    string[] listtrades = cme_reader.get_list_of_trades();
            //    cme_reader.extract_zip();
            //    // Get data from database.
            //    get_data_from_db();
            //}
            //else
            //{
            //    get_data_from_db();
            //}

            get_data_from_db();

            //CMEsdr cme_reader = new CMEsdr();
            //cme_reader.Connect();
            //cme_reader.retro_load("commodities");

            // Create a timer and set a 5-min interval.
            System.Timers.Timer aTimer = new System.Timers.Timer(60 * 5 * 1000);
            // Hook up the Elapsed event for the timer.
            aTimer.Elapsed += OnTimedEvent;
            //Start the timer.
            aTimer.Start();
        }

        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            // Clear previous record.
            //dataGridView1.Rows.Clear();

            CMEsdr cme_reader = new CMEsdr();
            // WIP & TODO: Needs to add real time update of Database using CME-reader.extract_zip()
            cme_reader.Connect();
            cme_reader.poll_list_of_trades("commodities");
            string[] listtrades = cme_reader.get_list_of_trades();
            cme_reader.extract_zip(listtrades[listtrades.Length-1]);

            //Do the stuff you want to be done every 5 minutes;
            get_data_from_db();

        }


        private void get_data_from_db()
        {
            // Get data from database.
            var list_of_trades = (from trade in db.CME_RawData
                                  where trade.Id >= current_Id
                                  select new
                                  {
                                      Id = trade.Id,
                                      Product = trade.Product,
                                      Market = trade.Product,
                                      Price = trade.Price,
                                      Maturity = trade.MaturityDate,
                                      ExecutionTime = trade.ExecutionTime,
                                      Event = trade.Event,
                                      Collaterization = trade.Collateralization
                                  }).Distinct().ToList();
            //if (list_of_trades.IsBlank())
            //{
            //    CMEsdr cme_reader = new CMEsdr();
            //    cme_reader.Connect();
            //    cme_reader.retro_load("commodities");
            //}


            // Get records in CME raw data.
            foreach (var cme in list_of_trades)
            {
                // Check if price is NULL.
                //if (!double.TryParse(cme.Price.ToString(), out double g))
                //{
                //    price = 0;
                //}
                //else
                //{
                //    price = Convert.ToDouble(cme.Price);
                //}

                // Check if collateralization is NULL. 
                //if (String.IsNullOrEmpty(cme.Collaterization.ToString()))
                //{
                //    collateral = "NA";
                //}
                //else
                //{
                //    collateral = cme.Collaterization;
                //}

                // Add rows to dataGridview.
                dataGridView1.Rows.Add(cme.Id,
                                       cme.Product,
                                       cme.Market,
                                       // price,
                                       cme.Price ?? 0,
                                       // operator ?? returns the value of its left-hand operand if it isn't null; 
                                       // otherwise, it evaluates the right-hand operand and returns its result.
                                       cme.Maturity,
                                       cme.ExecutionTime,
                                       cme.Event,
                                       cme.Collaterization == ""? "NA" : cme.Collaterization);
            }

            if (list_of_trades.Count > 0) // check if query returns anything...
            {
                current_Id = db.CME_RawData.Max(u => u.Id) + 1;
                //current_Id = (from q in db.CME_RawData
                //              select q.Id).Last() + 1;
            }
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }



        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            // Show detail data in database
            if (e.RowIndex >= 0)
            {

                //string Leg1Curr, Leg2Curr, Leg1In, Leg2In, Leg1Ty, Leg2Ty, Leg1TNotion, Leg2TNotion, Leg1TUnit, Leg2TUnit;
                //string OptionType, Iscall, Expiration, Strike, currency, premium;


                // Select responding row
                DataGridViewRow row = dataGridView1.Rows[e.RowIndex];
                string id = row.Cells["IdCol"].Value.ToString();

                // Get responding record in database.
                CME_RawData p = (from q in db.CME_RawData  // just change it to instrument after cleaning the data
                                 where q.Id.ToString() == id
                                 select q).First();


                // Assign the value to the text of GUI
                // If some of the columns are NULL, assgin NA to GUI
                //if (!double.TryParse(p.Leg1TotalNotional.ToString(), out double g))
                //{
                //    Leg1TNotion = "NA";
                //}
                //else
                //{
                //    Leg1TNotion = p.Leg1TotalNotional.ToString();
                //}

                //if (!double.TryParse(p.Leg2TotalNotional.ToString(), out double r))
                //{
                //    Leg2TNotion = "NA";
                //}
                //else
                //{
                //    Leg2TNotion = p.Leg2TotalNotional.ToString();
                //}

                //if (String.IsNullOrEmpty(p.Leg1Currency.ToString()))
                //{
                //    Leg1Curr = "NA";
                //}
                //else
                //{
                //    Leg1Curr = p.Leg1Currency.ToString();
                //}

                //if (String.IsNullOrEmpty(p.Leg2Currency.ToString()))
                //{
                //    Leg2Curr = "NA";
                //}
                //else
                //{
                //    Leg2Curr = p.Leg2Currency.ToString();
                //}

                //if (String.IsNullOrEmpty(p.Leg1Index.ToString()))
                //{
                //    Leg1In = "NA";
                //}
                //else
                //{
                //    Leg1In = p.Leg1Index.ToString();
                //}

                //if (String.IsNullOrEmpty(p.Leg2Index.ToString()))
                //{
                //    Leg2In = "NA";
                //}
                //else
                //{
                //    Leg2In = p.Leg2Index.ToString();
                //}

                //if (String.IsNullOrEmpty(p.Leg1Type.ToString()))
                //{
                //    Leg1Ty = "NA";
                //}
                //else
                //{
                //    Leg1Ty = p.Leg1Type.ToString();
                //}

                //if (String.IsNullOrEmpty(p.Leg2Type.ToString()))
                //{
                //    Leg2Ty = "NA";
                //}
                //else
                //{
                //    Leg2Ty = p.Leg2Type.ToString();
                //}

                //if (String.IsNullOrEmpty(p.Leg1TotalNotionalUnit.ToString()))
                //{
                //    Leg1TUnit = "NA";
                //}
                //else
                //{
                //    Leg1TUnit = p.Leg1TotalNotionalUnit.ToString();
                //}


                //if (String.IsNullOrEmpty(p.Leg2TotalNotionalUnit.ToString()))
                //{
                //    Leg2TUnit = "NA";
                //}
                //else
                //{
                //    Leg2TUnit = p.Leg2TotalNotionalUnit.ToString();
                //}

                //if (String.IsNullOrEmpty(p.OptionType.ToString()))
                //{
                //    OptionType = "NA";
                //}
                //else
                //{
                //    OptionType = p.OptionType.ToString();
                //}

                //if (String.IsNullOrEmpty(p.IsCall.ToString()))
                //{
                //    Iscall = "NA";
                //}
                //else
                //{
                //    Iscall = p.IsCall.ToString();
                //}

                //if (!double.TryParse(p.Strike.ToString(), out double k))
                //{
                //    Strike = "NA";
                //}
                //else
                //{
                //    Strike = p.Strike.ToString();
                //}

                //if (String.IsNullOrEmpty(p.OptionExpiration.ToString()))
                //{
                //    Expiration = "NA";
                //}
                //else
                //{
                //    Expiration = p.OptionExpiration.ToString();
                //}

                //if (String.IsNullOrEmpty(p.OptionCurrency.ToString()))
                //{
                //    currency = "NA";
                //}
                //else
                //{
                //    currency = p.OptionCurrency.ToString();
                //}

                //if (!double.TryParse(p.OptionPremium.ToString(), out double h))
                //{
                //    premium = "NA";
                //}
                //else
                //{
                //    premium = p.OptionPremium.ToString();
                //}



                // Assign all the values to GUI
                L1FixPay.Text = p.Leg1FixedPayment.ToString();
                // L1FixPayCurrency.Text = Leg1Curr;
                L1FixPayCurrency.Text = p.Leg1Currency == ""? "NA" : p.Leg1Currency;
                // L1Index.Text = Leg1In;
                L1Index.Text = p.Leg1Index == ""? "NA" : p.Leg1Index;
                L1Spread.Text = p.Leg1Spread.ToString();
                //L1TotalNotional.Text = Leg1TNotion;
                L1TotalNotional.Text = p.Leg1TotalNotional.HasValue ? p.Leg1TotalNotional.ToString() : "NA";
                //L1TotalNotionUnit.Text = Leg1TUnit;
                L1TotalNotionUnit.Text = p.Leg1TotalNotionalUnit == "" ? "NA" : p.Leg1TotalNotionalUnit;
                //L1Type.Text = Leg1Ty;
                L1Type.Text = p.Leg1Type == "" ? "NA" : p.Leg1Type;

                L2FixPay.Text = p.Leg2FixedPayment.ToString();
                //L2FixPayCurrency.Text = Leg2Curr;
                L2FixPayCurrency.Text = p.Leg2Currency == "" ? "NA" : p.Leg2Currency;
                //L2Index.Text = Leg2In;
                L2Index.Text = p.Leg2Index == "" ? "NA" : p.Leg2Index;
                L2Spread.Text = p.Leg2Spread.ToString();
                //L2TotalNational.Text = Leg2TNotion;
                L2TotalNational.Text = p.Leg2TotalNotional.ToString();
                L2TotalNotionUnit.Text = p.Leg2TotalNotionalUnit == "" ? "NA" : p.Leg2TotalNotionalUnit;
                //L2Type.Text = Leg2Ty;
                L2Type.Text = p.Leg2Type == "" ? "NA" : p.Leg2Type;

                DataSource.Text = "CME"; // Change it to p.DataSource later.
                Effect.Text = p.EffectiveDate.ToString();
                SettleCurrency.Text = p.SettleCurrency.ToString();
                SettleMethod.Text = p.SettleMethod.ToString();
                Cleared.Text = p.Cleared.ToString();
                ExeVenue.Text = p.ExecutionVenue.ToString();
                BlockLg.Text = p.BlockOff.ToString();
                EndExp.Text = p.End_User_Exception.ToString();

                //OpType.Text = OptionType;
                //OpIsCall.Text = Iscall;
                //OpExp.Text = Expiration;
                //OpStrike.Text = Strike;
                //OpCurrency.Text = currency;
                //OpPremium.Text = premium;
                OpType.Text = p.OptionType == "" ? "NA" : p.OptionType;
                OpIsCall.Text = p.IsCall == "" ? "NA" : p.IsCall;
                OpExp.Text = p.OptionExpiration.HasValue ? p.OptionExpiration.ToString() : "NA";
                OpStrike.Text = p.Strike.HasValue ? p.Strike.ToString() : "NA";
                OpCurrency.Text = p.OptionCurrency == "" ? "NA" : p.OptionCurrency;
                OpPremium.Text = p.OptionPremium.HasValue ? p.OptionPremium.ToString() : "NA";
            }

        }

    }
}

