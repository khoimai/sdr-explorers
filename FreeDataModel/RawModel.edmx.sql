
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/18/2019 11:40:13
-- Generated from EDMX file: C:\Users\darkm\Documents\repos\sdr-explorers\FreeDataModel\RawModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [FreeModelDB];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_Swaps_inherits_Instrument]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Instruments_Swaps] DROP CONSTRAINT [FK_Swaps_inherits_Instrument];
GO
IF OBJECT_ID(N'[dbo].[FK_Options_inherits_Instrument]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Instruments_Options] DROP CONSTRAINT [FK_Options_inherits_Instrument];
GO
IF OBJECT_ID(N'[dbo].[FK_Forwards_inherits_Instrument]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Instruments_Forwards] DROP CONSTRAINT [FK_Forwards_inherits_Instrument];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[CME_RawData]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CME_RawData];
GO
IF OBJECT_ID(N'[dbo].[DTCC_RawData]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DTCC_RawData];
GO
IF OBJECT_ID(N'[dbo].[Instruments]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Instruments];
GO
IF OBJECT_ID(N'[dbo].[Commodities]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Commodities];
GO
IF OBJECT_ID(N'[dbo].[Instruments_Swaps]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Instruments_Swaps];
GO
IF OBJECT_ID(N'[dbo].[Instruments_Options]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Instruments_Options];
GO
IF OBJECT_ID(N'[dbo].[Instruments_Forwards]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Instruments_Forwards];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'CME_RawData'
CREATE TABLE [dbo].[CME_RawData] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Collateralization] nvarchar(max)  NULL,
    [ExecutionTime] datetime  NOT NULL,
    [Cleared] nvarchar(max)  NULL,
    [End_User_Exception] nvarchar(max)  NULL,
    [Bespoke] bit  NOT NULL,
    [BlockOff] bit  NOT NULL,
    [Product] nvarchar(max)  NOT NULL,
    [Contract_Type] nvarchar(max)  NOT NULL,
    [EffectiveDate] datetime  NOT NULL,
    [MaturityDate] datetime  NOT NULL,
    [ExecutionVenue] nvarchar(max)  NOT NULL,
    [SettleMethod] nvarchar(max)  NOT NULL,
    [SettleCurrency] nvarchar(max)  NOT NULL,
    [Leg1Type] nvarchar(max)  NULL,
    [Leg1FixedPayment] float  NOT NULL,
    [Leg1Currency] nvarchar(max)  NULL,
    [Leg1Index] nvarchar(max)  NULL,
    [Leg1Spread] float  NOT NULL,
    [Leg2Type] nvarchar(max)  NULL,
    [Leg2FixedPayment] float  NOT NULL,
    [Leg2Currency] nvarchar(max)  NULL,
    [Leg2Index] nvarchar(max)  NULL,
    [Leg2Spread] float  NOT NULL,
    [Leg1TotalNotionalUnit] nvarchar(max)  NOT NULL,
    [Leg2TotalNotional] float  NOT NULL,
    [Leg2TotalNotionalUnit] nvarchar(max)  NULL,
    [ContractSubtype] nvarchar(max)  NULL,
    [Event] nvarchar(max)  NOT NULL,
    [Strike] float  NULL,
    [OptionType] nvarchar(max)  NULL,
    [OptionCurrency] nvarchar(max)  NULL,
    [OptionPremium] float  NULL,
    [IsCall] nvarchar(max)  NULL,
    [OptionExpiration] datetime  NULL,
    [Leg1IndexLocation] nvarchar(max)  NULL,
    [Price] float  NULL,
    [Leg1TotalNotional] float  NULL,
    [RptID] nvarchar(max)  NOT NULL,
    [ModifiedDate_META] datetime  NOT NULL
);
GO

-- Creating table 'DTCC_RawData'
CREATE TABLE [dbo].[DTCC_RawData] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Action] nvarchar(max)  NULL,
    [ExecutionTime] datetime  NOT NULL,
    [Cleared] nvarchar(max)  NULL,
    [Collateralization] nvarchar(max)  NULL,
    [End_User_Exception] nvarchar(max)  NULL,
    [OtherPriceAffecting] bit  NOT NULL,
    [BlockOff] bit  NOT NULL,
    [ExecutionVenue] nvarchar(max)  NOT NULL,
    [EffectiveDate] nvarchar(max)  NOT NULL,
    [MaturityDate] nvarchar(max)  NOT NULL,
    [SettleCurrency] nvarchar(max)  NULL,
    [Taxonomy] nvarchar(max)  NULL,
    [Underlying] nvarchar(max)  NULL,
    [PriceNotationType] nvarchar(max)  NULL,
    [PriceNotation] float  NULL,
    [NotionalCurrency1] nvarchar(max)  NULL,
    [NotionalCurrency2] nvarchar(max)  NULL,
    [NotionalAmount1] float  NULL,
    [NotionalAmount2] float  NULL,
    [Strike] float  NULL,
    [IsCall] nvarchar(max)  NULL,
    [OptionCurrency] nvarchar(max)  NULL,
    [OptionType] nvarchar(max)  NULL,
    [OptionPremium] float  NULL,
    [OptionExpiration] datetime  NULL
);
GO

-- Creating table 'Instruments'
CREATE TABLE [dbo].[Instruments] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [DataSource] nvarchar(max)  NOT NULL,
    [Market] nvarchar(max)  NULL,
    [Event] nvarchar(max)  NULL,
    [ContractType] nvarchar(max)  NULL,
    [EffectiveDate] datetime  NULL,
    [MaturityDate] datetime  NULL,
    [ExecutionVenue] nvarchar(max)  NULL,
    [BlockOff] bit  NULL,
    [End_User_Exception] nvarchar(max)  NULL,
    [SettleCurrency] nvarchar(max)  NULL,
    [SettleMethod] nvarchar(max)  NULL,
    [Collateralization] nvarchar(max)  NULL,
    [ContractSubtype] nvarchar(max)  NULL,
    [Price] float  NULL,
    [SourceID] int  NOT NULL
);
GO

-- Creating table 'Commodities'
CREATE TABLE [dbo].[Commodities] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Instruments_Swaps'
CREATE TABLE [dbo].[Instruments_Swaps] (
    [Leg1Type] nvarchar(max)  NULL,
    [Leg1FixedPayment] float  NULL,
    [Leg1Currency] nvarchar(max)  NULL,
    [Leg1Index] nvarchar(max)  NOT NULL,
    [Leg1Spread] float  NULL,
    [Leg2Type] nvarchar(max)  NULL,
    [Leg2FixedPayment] float  NULL,
    [Leg2Index] nvarchar(max)  NULL,
    [Leg2Spread] float  NULL,
    [Leg1TotalNotionUnit] nvarchar(max)  NOT NULL,
    [Leg2TotalNotional] float  NULL,
    [Leg2TotalNotionUnit] nvarchar(max)  NULL,
    [Leg1TotalNotional] float  NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'Instruments_Options'
CREATE TABLE [dbo].[Instruments_Options] (
    [OptionType] nvarchar(max)  NULL,
    [Strike] float  NULL,
    [Currency] nvarchar(max)  NULL,
    [Premium] float  NULL,
    [Expiration] datetime  NULL,
    [IsCall] nvarchar(max)  NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'Instruments_Forwards'
CREATE TABLE [dbo].[Instruments_Forwards] (
    [FixedPayment] float  NULL,
    [PaymentCurrency] nvarchar(max)  NULL,
    [Index] nvarchar(max)  NULL,
    [Spread] float  NULL,
    [TotalNotionUnit] nvarchar(max)  NULL,
    [TotalNotion] float  NULL,
    [Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'CME_RawData'
ALTER TABLE [dbo].[CME_RawData]
ADD CONSTRAINT [PK_CME_RawData]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'DTCC_RawData'
ALTER TABLE [dbo].[DTCC_RawData]
ADD CONSTRAINT [PK_DTCC_RawData]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Instruments'
ALTER TABLE [dbo].[Instruments]
ADD CONSTRAINT [PK_Instruments]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Commodities'
ALTER TABLE [dbo].[Commodities]
ADD CONSTRAINT [PK_Commodities]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Instruments_Swaps'
ALTER TABLE [dbo].[Instruments_Swaps]
ADD CONSTRAINT [PK_Instruments_Swaps]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Instruments_Options'
ALTER TABLE [dbo].[Instruments_Options]
ADD CONSTRAINT [PK_Instruments_Options]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Instruments_Forwards'
ALTER TABLE [dbo].[Instruments_Forwards]
ADD CONSTRAINT [PK_Instruments_Forwards]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Id] in table 'Instruments_Swaps'
ALTER TABLE [dbo].[Instruments_Swaps]
ADD CONSTRAINT [FK_Swaps_inherits_Instrument]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Instruments]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'Instruments_Options'
ALTER TABLE [dbo].[Instruments_Options]
ADD CONSTRAINT [FK_Options_inherits_Instrument]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Instruments]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'Instruments_Forwards'
ALTER TABLE [dbo].[Instruments_Forwards]
ADD CONSTRAINT [FK_Forwards_inherits_Instrument]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Instruments]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------