//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FreeDataModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class Instrument
    {
        public int Id { get; set; }
        public string DataSource { get; set; }
        public string Market { get; set; }
        public string Event { get; set; }
        public string ContractType { get; set; }
        public Nullable<System.DateTime> EffectiveDate { get; set; }
        public Nullable<System.DateTime> MaturityDate { get; set; }
        public string ExecutionVenue { get; set; }
        public Nullable<bool> BlockOff { get; set; }
        public string End_User_Exception { get; set; }
        public string SettleCurrency { get; set; }
        public string SettleMethod { get; set; }
        public string Collateralization { get; set; }
        public string ContractSubtype { get; set; }
        public Nullable<double> Price { get; set; }
        public int SourceID { get; set; }
    }
}
