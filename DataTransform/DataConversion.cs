﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FreeDataModel;
using Commodities;

namespace DataTransform
{
    public class DataConversion
    {
        /// <summary>
        /// Takes raw data from CME table, cleans it up and writes to the instrument table
        /// </summary>
        public static void ConvertCME()
        {
            CommodityLibrary CLibrary = new CommodityLibrary();
            const string data_source = "CME";

            using(RawModelContainer RMC = new RawModelContainer())
            {
                var completedtrade_ids = from i in RMC.Instruments where i.DataSource == data_source select i.SourceID; //source ids for cleaned trades
                //remove trades that have already been cleaned
                var NewTrades = from i in RMC.CME_RawData
                                 where !completedtrade_ids.Contains(i.Id)
                                 select i;
                //pick out each contract type 
                var newoptions = from i in NewTrades where i.Contract_Type == "Option" select i;
                var newswaps = from i in NewTrades
                               where i.Contract_Type == "CommoditySwap" || i.Contract_Type == "Commodity Swap"
                               select i;
                var newforwards = from i in NewTrades where i.Contract_Type == "Forward" select i;

                //loops to assign properties
                foreach(var o in newoptions)
                {
                    string market = CLibrary.ConvertProduct(o.Product);

                    Options op = new Options()
                    {
                        //assign props
                        DataSource = data_source,
                        Market = market,
                        Event = o.Event,
                        ContractType = "Option",
                        EffectiveDate = o.EffectiveDate,
                        MaturityDate = o.MaturityDate,
                        ExecutionVenue = o.ExecutionVenue,
                        BlockOff = o.BlockOff,
                        End_User_Exception = o.End_User_Exception,
                        SettleCurrency = o.SettleCurrency,
                        SettleMethod = o.SettleMethod,
                        Collateralization = o.Collateralization,
                        ContractSubtype = o.ContractSubtype,
                        Price = o.Price,
                        SourceID = o.Id,
                        //option props
                        OptionType = o.OptionType,
                        Strike = o.Strike,
                        Currency = o.OptionCurrency,
                        Premium = o.OptionPremium,
                        Expiration = o.OptionExpiration,
                        IsCall = o.IsCall
                    };
                    RMC.Instruments.Add(op);
                }
                foreach(var s in newswaps)
                {
                    string market = CLibrary.ConvertProduct(s.Product);

                    Swaps sp = new Swaps()
                    {
                        //assign props//assign props
                        DataSource = data_source,
                        Market = market,
                        Event = s.Event,
                        ContractType = "CommoditySwap",
                        EffectiveDate = s.EffectiveDate,
                        MaturityDate = s.MaturityDate,
                        ExecutionVenue = s.ExecutionVenue,
                        BlockOff = s.BlockOff,
                        End_User_Exception = s.End_User_Exception, //assigning this many props is horrible
                        SettleCurrency = s.SettleCurrency, //I wish I knew more about reflection
                        SettleMethod = s.SettleMethod,
                        Collateralization = s.Collateralization,
                        ContractSubtype = s.ContractSubtype,
                        Price = s.Price,
                        SourceID = s.Id,
                        //swap prpps
                        Leg1Type = s.Leg1Type,
                        Leg1TotalNotional = s.Leg1TotalNotional,
                        Leg1Spread = s.Leg1Spread,
                        Leg1Index = s.Leg1Index,
                        Leg1Currency = s.Leg1Currency,
                        Leg1FixedPayment = s.Leg1FixedPayment,
                        Leg1TotalNotionUnit = s.Leg1TotalNotionalUnit,
                        Leg2FixedPayment = s.Leg2FixedPayment,
                        Leg2Index = s.Leg2Index,
                        Leg2Spread = s.Leg2Spread,
                        Leg2TotalNotional = s.Leg2TotalNotional,
                        Leg2Type = s.Leg2Type,
                        Leg2TotalNotionUnit = s.Leg2TotalNotionalUnit
                    };
                    RMC.Instruments.Add(sp);
                }
                foreach(var f in newforwards)
                {
                    string market = CLibrary.ConvertProduct(f.Product);
                    Forwards fd = new Forwards()
                    {
                        //assign props
                        DataSource = data_source,
                        Market = market,
                        Event = f.Event,
                        ContractType = "CommoditySwap",
                        EffectiveDate = f.EffectiveDate,
                        MaturityDate = f.MaturityDate,
                        ExecutionVenue = f.ExecutionVenue,
                        BlockOff = f.BlockOff,
                        End_User_Exception = f.End_User_Exception,
                        SettleCurrency = f.SettleCurrency,
                        SettleMethod = f.SettleMethod,
                        Collateralization = f.Collateralization,
                        ContractSubtype = f.ContractSubtype,
                        Price = f.Price,
                        SourceID = f.Id,
                        //forwards props
                        FixedPayment = f.Leg1FixedPayment,
                        PaymentCurrency = f.Leg1Currency,
                        Index = f.Leg1Index,
                        Spread = f.Leg1Spread,
                        TotalNotionUnit = f.Leg1TotalNotionalUnit,
                        TotalNotion = f.Leg1TotalNotional
                    };
                    RMC.Instruments.Add(fd);
                }
                RMC.SaveChanges();
            }
        }
        
        public static void ConvertDTCC()
        {
            CommodityLibrary CLibrary = new CommodityLibrary();
            const string data_source = "DTCC";

            using(RawModelContainer RMC = new RawModelContainer())
            {

            }
        }

        public static void ConvertICE()
        {
            CommodityLibrary CLibrary = new CommodityLibrary();
            const string data_source = "ICE";

            using(RawModelContainer RMC = new RawModelContainer())
            {

            }
        }
    }
}
