﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentFTP;
using System.IO;
using System.IO.Compression;
using Microsoft.VisualBasic.FileIO;
using FreeDataModel;
using System.Text.RegularExpressions;

namespace CME_reader
{
    public class CMEsdr
    {
        FtpClient client_cme = new FtpClient("ftp://ftp.cmegroup.com");

        Dictionary<string, string> CME_string_mapping = new Dictionary<string, string>()
            {
                { "commodities" , "COMMODITY" },
                { "rates", "IRS" },
                { "fx" , "FX" },
                { "credit" , "CDS" } // only has data of 2014 and 2015 btw...
            };

        private string Connect_Exception;
        private string[] list_of_trades;

        public string get_Connect_Exception()
        {
            return Connect_Exception;
        }

        public CMEsdr()
        {
        }

        public void Connect()
        {
            try
            {
                client_cme.Connect();
            }
            catch (Exception ex)
            {
                Connect_Exception = ex.GetType().Name;
            }
        }

        // type - "commodities", "credit", "fx", "rates"
        public void poll_list_of_trades(string type)
        {
            // Convert to Central Time, since CME is in Chicago
            DateTime timeUtc = DateTime.UtcNow;
            TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
            DateTime cstTime = TimeZoneInfo.ConvertTimeFromUtc(timeUtc, cstZone);

            string year = cstTime.Year.ToString();
            string month = cstTime.Month.ToString("00");

            list_of_trades = client_cme.GetNameListing("/sdr/" + type + "/" + year + "/" + month);
        }

        public string [] get_list_of_trades()
        {
            return list_of_trades;
        }

        public void retro_load(string type)
        {
            DateTime timeUtc = DateTime.UtcNow;
            TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
            DateTime cstTime = TimeZoneInfo.ConvertTimeFromUtc(timeUtc, cstZone);

            // Checking the last row of data in database
            using (RawModelContainer db = new RawModelContainer())
            {
                var latest_date_in_db_query = from trade in db.CME_RawData
                                              orderby trade.ModifiedDate_META descending
                                              select trade;

                if (latest_date_in_db_query.ToList().Count > 0)
                {
                    DateTime latest_date_in_db = latest_date_in_db_query.FirstOrDefault().ModifiedDate_META;
                    if (latest_date_in_db.Date == cstTime.Date)
                    {
                        List<string> list_of_current_month = client_cme.GetNameListing(
                                                         "/sdr/" + type + "/" + 
                                                         latest_date_in_db.Year + "/" + 
                                                         latest_date_in_db.Month.ToString("00")).ToList();

                        // regex to search for pattern /sdr/<type>/<yyyy>/<MM>/RT.<CME_string_mapping[type]>.<yyyyMMdd>.****.csv.zip
                        // where "<CME_string_mapping[type]" is the dictionary defined globally above,
                        // and "<type>" consists of "commodities", "credit", "fx", "rates"
                        // E.G: /sdr/commodities/2019/07/RT.COMMODITY.20190702.0000.csv.zip

                        var myRegex = new Regex(@"^/sdr/" + type + "/" +
                                                latest_date_in_db.Year + "/" +
                                                latest_date_in_db.Month.ToString("00") + "/" + 
                                                "RT." + CME_string_mapping[type] + "." + 
                                                latest_date_in_db.ToString("yyyyMMdd") + "." + @"\d+" + ".csv.zip");

                        // do the searching
                        List<string> resultList = list_of_current_month.Where(f => myRegex.IsMatch(f)).ToList();

                        if (!resultList.IsBlank())
                        {
                            foreach (string file_path in resultList)
                            {
                                extract_zip(file_path);
                            }
                        }
                    }
                    else if (latest_date_in_db.Date < cstTime.Date)
                    {
                        while (latest_date_in_db.Date < cstTime.Date)
                        {
                            // Format: /sdr/commodities/2019/07/RT.COMMODITY.20190702.csv.zip
                            string target_string = "/sdr/" + type + "/" +
                                                    latest_date_in_db.Year.ToString() + "/" +
                                                    latest_date_in_db.Month.ToString("00") + "/" +
                                                    "RT." + CME_string_mapping[type] + "." +
                                                    latest_date_in_db.ToString("yyyyMMdd") + ".csv.zip";

                            //if (temp.Where(x => x.Contains(target_string)).ToList().Count > 0)
                            if (client_cme.FileExists(target_string))
                            {
                                extract_zip(target_string);
                            }

                            latest_date_in_db = latest_date_in_db.AddDays(1);
                        }
                    }
                }
                else // nothing in database...
                {
                    // load everything from the beginning of time! Jk jk
                    // try loading data from previous month...
                    DateTime last_month_date = cstTime.AddMonths(-1);
                    string[] last_month_list = client_cme.GetNameListing(
                                                         "/sdr/" + type + "/" +
                                                         last_month_date.Year + "/" +
                                                         last_month_date.Month.ToString("00"));
                    foreach(string file_path in last_month_list)
                    {
                        extract_zip(file_path);
                    }
                }
            }
        }

        public void extract_zip(string file_path)
        {
            using (Stream zip_stream = new MemoryStream())
            {
                //if (client_cme.Download(zip_stream, list_of_trades[list_of_trades.Length - 1]))
                if (client_cme.Download(zip_stream, file_path))
                {
                    DateTime modified_time = client_cme.GetModifiedTime(file_path);
                    using (ZipArchive archive = new ZipArchive(zip_stream))
                    {
                        foreach (ZipArchiveEntry entry in archive.Entries)
                        {
                            var stream = entry.Open();
                            using (TextFieldParser parser = new TextFieldParser(new StreamReader(stream)))
                            {
                                parser.TextFieldType = FieldType.Delimited;
                                parser.SetDelimiters(",");

                                // For reading headers
                                string[] headers;

                                using (RawModelContainer db = new RawModelContainer())
                                {
                                    if (!parser.EndOfData)
                                    {
                                        headers = parser.ReadFields();
                                    }

                                    while (!parser.EndOfData)
                                    {
                                        //Process row
                                        string[] fields = parser.ReadFields();
                                        //temp.Product = fields[10];
                                        //temp.Price = Convert.ToDouble(fields[17]);
                                        //temp.MaturityDate = DateTime.Parse(fields[14]);
                                        //temp.ExecutionTime = DateTime.Parse(fields[1]);
                                        //temp.Event = fields[0];
                                        //list_of_display_data.Add(temp);
                                        string report_ID = fields[44];
                                        if (db.CME_RawData.Count(a => a.RptID == report_ID) == 0) // check if RptID already exists, if not, then insert
                                        {
                                            db.CME_RawData.Add(new CME_RawData()
                                            {
                                                Event = fields[0],
                                                ExecutionTime = DateTime.Parse(fields[1]),
                                                Cleared = fields[3],
                                                Collateralization = fields[4],
                                                End_User_Exception = fields[5],
                                                Bespoke = fields[6] == "Yes" ? true : false,
                                                BlockOff = fields[7] == "Yes" ? true : false,
                                                ExecutionVenue = fields[8],
                                                Product = fields[10],
                                                Contract_Type = fields[12],
                                                EffectiveDate = DateTime.Parse(fields[13]),
                                                MaturityDate = DateTime.Parse(fields[14]),
                                                Leg1TotalNotional = fields[16] == "" ? (double?)null : Convert.ToDouble(fields[16]),
                                                Price = fields[17] == "" ? (double?)null : Convert.ToDouble(fields[17]),
                                                SettleMethod = fields[18],
                                                SettleCurrency = fields[19],
                                                Leg1Type = fields[20],
                                                Leg1FixedPayment = Convert.ToDouble(fields[21]),
                                                Leg1Currency = fields[22],
                                                Leg1Index = fields[23],
                                                Leg1IndexLocation = fields[24],
                                                Leg1Spread = Convert.ToDouble(fields[25]),
                                                Leg2Type = fields[28],
                                                Leg2FixedPayment = Convert.ToDouble(fields[29]),
                                                Leg2Currency = fields[30],
                                                Leg2Index = fields[31],
                                                Leg2Spread = Convert.ToDouble(fields[33]),
                                                Strike = Convert.ToDouble(fields[37]),
                                                IsCall = fields[38],
                                                OptionType = fields[39],
                                                OptionCurrency = fields[40],
                                                OptionPremium = fields[41] == "" ? (double?)null : Convert.ToDouble(fields[41]), // Troublesome dude right here - Khoi
                                                OptionExpiration = fields[43] == "" ? (DateTime?)null : DateTime.Parse(fields[43]),
                                                RptID = fields[44],
                                                Leg1TotalNotionalUnit = fields[47],
                                                Leg2TotalNotional = Convert.ToDouble(fields[48]),
                                                Leg2TotalNotionalUnit = fields[49],
                                                ContractSubtype = fields[63],
                                                ModifiedDate_META = modified_time // for book-keeping...
                                            });
                                        }
                                    }
                                    db.SaveChanges();
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
